.PROGRAM ros_srv_fdb()

; Sending joint feedback to ROS.
; 27\02\2021 by Miglans
; Declaration tags
        AUTO slun, status
        AUTO REAL jts[10]
        AUTO LOC #cur.loc
        AUTO $msg
 ; Open a Serial port
        ATTACH (slun, 4) "LOCAL.SERIAL:4"
        status = IOSTAT(slun)
        IF (status < 0) THEN
            TYPE "Error opening Serial port: "+$ERROR(status)
            GOTO 100
        END
        WRITE (slun) "Connected to PC by RS232"
 ; Server loop
        WHILE run DO

 ; Reset status
            $msg = ""

 ; Get the current joint position
            HERE #cur.loc
            DECOMPOSE jts[1] = #cur.loc
; Send the message to ROS
            $msg = $ENCODE(jts[1]," ",jts[2]," ",jts[3]," ",jts[4]," ",jts[5],"
            WRITE (slun) $msg

; Loop delay
            WAIT.EVENT , delay.fdb
        END

 ; Close the Serial port
        DETACH (slun)
; Return
   100  RETURN
